#include <iostream>

#include <stdlib.h>

#include <aws/core/utils/crypto/HashResult.h>
#include <aws/core/utils/crypto/Hash.h>

#include <aws/core/utils/ratelimiter/DefaultRateLimiter.h>


#include <aws/swf/SWFErrors.h>
#include <aws/swf/SWF_EXPORTS.h>
#include <aws/swf/SWFClient.h>
#include <aws/swf/SWFEndpoint.h>
#include <aws/swf/SWFErrorMarshaller.h>
#include <aws/swf/SWFRequest.h>

#include <aws/swf/model/StartWorkflowExecutionRequest.h>
#include <aws/swf/model/PollForDecisionTaskRequest.h>
#include <aws/swf/model/PollForActivityTaskRequest.h>
#include <aws/swf/model/RespondDecisionTaskCompletedRequest.h>
#include <aws/swf/model/RespondActivityTaskCompletedRequest.h>
#include <aws/core/utils/base64/Base64.h>
#include <aws/core/utils/json/JsonSerializer.h>
#include <aws/core/utils/Array.h>


//#include <aws/swf/model/TaskList.h>
#include <aws/acm/ACMClient.h>
#include <aws/core/Aws.h>
#include <aws/core/utils/Outcome.h>


#include <zconf.h>
#include <iterator>
//#include <aws/s3/S3Client.h>

#include "MyDecision.h"

using namespace std;
using namespace Aws;
using namespace Aws::SWF;
using namespace Aws::SWF::Model;
using namespace Aws::Utils;
using namespace Aws::Utils::Base64;


auto domain = "play";
auto awstask = "./awstask";

enum ToolType { decider, workstarter, worker, executor };

ToolType parseToolType(int argc, char **argv);


void decide(SWFClient &swfClient);

void startWork(SWFClient &swfClient);

void execute(SWFClient &swfClient);


WorkflowType &getWorkflowType(WorkflowType &workflowType);
TaskList &getTaskList(TaskList &taskList);
string runAwsTask(const char domain[5], TaskList list, ActivityType type, const char* taskInput);
void taskCompleted(const SWFClient &swfClient, const String &taskToken, const String &out);
void taskCompleted(const SWFClient &swfClient, const String &taskToken);

void work(SWFClient swfClient);


int main(int argc, char* argv[]) {

    ToolType toolType = parseToolType(argc, argv);

    SDKOptions options;
    InitAPI(options);


    static const char* ALLOCATION_TAG = "swfplay";
    //static shared_ptr<Utils::RateLimits::RateLimiterInterface> Limiter;

    Client::ClientConfiguration config;


// Create a client
    config.scheme = Http::Scheme::HTTPS;
    config.connectTimeoutMs = 30000;
    config.requestTimeoutMs = 30000;
    config.region = "eu-central-1";

    /*auto limiter = MakeShared<Utils::RateLimits::DefaultRateLimiter<>>(ALLOCATION_TAG, 200000);
    config.readRateLimiter = limiter;
    config.writeRateLimiter = limiter;*/

    auto client = MakeShared<SWFClient>(ALLOCATION_TAG, config);

    switch (toolType) {

        case decider:
            cerr << "decider" << endl;
            decide(*client.get());
            break;
        case workstarter:
            cerr << "startWork" << endl;
            startWork(*client.get());
            break;
        case worker:
            cerr << "worker" << endl;
            work(*client.get());
            break;
        case executor:
        default:
            cerr << "executor" << endl;
            execute(*client.get());
            break;
    }

    cerr << "done" << endl;

    ShutdownAPI(options);

}

void work(SWFClient swfClient) {

    //istream_iterator<char> begin(cin);
    //istream_iterator<char> end;
    //string jsonBuffer(begin, end);

    Json::JsonValue jsonValue(cin);

    auto token = jsonValue.GetString("task_token");
    auto input = jsonValue.GetObject("input");
    auto timestamp = input.GetString("timestamp");
    cerr << "token " << token << endl;
    taskCompleted(swfClient, token, timestamp);

}

void taskCompleted(const SWFClient &swfClient, const String &taskToken) {
    String out("");
    taskCompleted(swfClient, taskToken, out);
}
void taskCompleted(const SWFClient &swfClient, const String &taskToken, const String &out) {
    RespondActivityTaskCompletedRequest completedRequest;
    completedRequest.SetTaskToken(taskToken);
    completedRequest.SetResult(out.c_str());
    RespondActivityTaskCompletedOutcome completedOutcome =
                swfClient.RespondActivityTaskCompleted(completedRequest);

    if (!completedOutcome.IsSuccess()) {
            throw runtime_error(completedOutcome.GetError().GetMessage().c_str());
        }
}

void execute(SWFClient &swfClient) {

    StartWorkflowExecutionRequest execRequest;

    execRequest.SetDomain(domain);
    WorkflowType workflowType;
    execRequest.SetWorkflowType(getWorkflowType(workflowType));
    TaskList taskList;
    execRequest.SetTaskList(getTaskList(taskList));
    execRequest.SetWorkflowId("play1");

    cout <<  "creating workflow execution: " << execRequest.GetWorkflowId() << endl;
    auto response = swfClient.StartWorkflowExecution(execRequest);
    if (response.IsSuccess()) {
        cout <<  "run id: " << response.GetResult().GetRunId() << endl;
    } else {
        cout <<  "error: " << response.GetError().GetExceptionName() << ", " << response.GetError().GetMessage() << endl;
    }
}

void startWork(SWFClient &swfClient) {

    PollForActivityTaskRequest activityTaskRequest;
    activityTaskRequest.SetDomain(domain);
    TaskList taskList;
    activityTaskRequest.SetTaskList(getTaskList(taskList));
    auto activityTaskOutcome = swfClient.PollForActivityTask(activityTaskRequest);
    if (0 == activityTaskOutcome.GetResult().GetTaskToken().compare("")) {
        cerr <<  "nothing to do" << endl;
    } else {
        Aws::Utils::Base64::Base64 base64;

        auto taskToken = activityTaskOutcome.GetResult().GetTaskToken();
        ByteBuffer taskTokenArray((unsigned char *)taskToken.c_str(), taskToken.length());
        String encodedTaskToken = base64.Encode(taskTokenArray);

        auto activityType = activityTaskOutcome.GetResult().GetActivityType();
        auto activityTypeName = activityType.GetName();
        auto activityTypeVersion = activityType.GetVersion();
        ByteBuffer activityTypeNameArray((unsigned char *)activityTypeName.c_str(), activityTypeName.length());
        ByteBuffer activityTypeVersionArray((unsigned char *)activityTypeVersion.c_str(), activityTypeVersion.length());
        String encodedActivityTypeName = base64.Encode(activityTypeNameArray);
        String encodedActivityTypeVersion = base64.Encode(activityTypeVersionArray);

        auto taskInput = activityTaskOutcome.GetResult().GetInput();
        ByteBuffer taskInputArray((unsigned char *)taskInput.c_str(), taskInput.length());
        String encodedTaskInput = base64.Encode(taskInputArray);

        cout << "activity_type_name_base64=" << encodedActivityTypeName << endl;
        cout << "activity_type_version_base64=" << encodedActivityTypeVersion << endl;
        cout << "task_input_base64=" << encodedTaskInput << endl;
        cout << "task_token_base64=" << encodedTaskToken << endl;

        //taskCompleted(swfClient, taskInput);
    }

}

string runAwsTask(const char domain[5], TaskList list, ActivityType type, const char* taskInput) {

    stringstream commandstream;
    commandstream
        << awstask
        << " " << domain
        << " " << list.GetName()
        << " " << type.GetName()
        << " " << taskInput;
    auto command = commandstream.str();


    cout << "running: " << command;

    char buffer[128];
    string result = "";
    shared_ptr<FILE> pipe(popen(command.c_str(), "r"), pclose);
    if (!pipe) throw runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }

    return result;
}

void decide(SWFClient &swfClient) {

    cout <<  "start decider" << endl;

    PollForDecisionTaskRequest decisionTaskRequest;

    decisionTaskRequest.SetDomain(domain);
    TaskList taskList;
    decisionTaskRequest.SetTaskList(getTaskList(taskList));

    cout <<  "PollForDecisionTask" << endl;
    auto decisionTaskOutcome = swfClient.PollForDecisionTask(decisionTaskRequest);
    if (0 == decisionTaskOutcome.GetResult().GetTaskToken().compare("")) {
        cout <<  "nothing to decide" << endl;
    } else {
        cout <<  "deciding " << decisionTaskOutcome.GetResult().GetTaskToken() << endl;
        MyDecision decision(decisionTaskOutcome.GetResult());
        decision.respondDecisionTaskCompleted(swfClient);
    }
}

ToolType parseToolType(int argc, char **argv) {

    if (argc > 1) {
        if (0 == strcmp("decider", argv[1])) {
            return ToolType::decider;
        } else if (0 == strcmp("executor", argv[1])) {
            return ToolType::executor;
        } else if (0 == strcmp("worker", argv[1])) {
            return ToolType::worker;
        } else if (0 == strcmp("workstarter", argv[1])) {
            return ToolType::workstarter;
        }
    }
    return executor;
}

WorkflowType &
getWorkflowType(WorkflowType &workflowType) {
    workflowType.SetName(domain);
    workflowType.SetVersion("1");
    return workflowType;
}

TaskList &getTaskList(TaskList &taskList) {
    String taskListName("default");
    taskList.SetName(taskListName);
    return taskList;
}
