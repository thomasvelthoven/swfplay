//
// Created by thomas on 19.09.16.
//

#include <aws/swf/model/EventType.h>
#include <aws/swf/model/RespondDecisionTaskCompletedRequest.h>
#include <aws/swf/model/TerminateWorkflowExecutionRequest.h>
#include <aws/swf/model/ScheduleActivityTaskDecisionAttributes.h>
#include <aws/swf/model/Decision.h>
#include <aws/swf/model/DecisionType.h>

#include "MyDecision.h"

Aws::SWF::Model::TaskList &getTaskList(Aws::SWF::Model::TaskList &taskList);


MyDecision::MyDecision(Aws::SWF::Model::PollForDecisionTaskResult &pollForDecisionTaskResult)
   :pollForDecisionTaskResult(pollForDecisionTaskResult),
    workflowExecution(pollForDecisionTaskResult.GetWorkflowExecution())
{

    std::cout
            << "starting decision task "
            << pollForDecisionTaskResult.GetTaskToken()
            << " on "
            << workflowExecution.GetWorkflowId()
            << " run "
            << workflowExecution.GetRunId()
            << std::endl;


    for (auto event : pollForDecisionTaskResult.GetEvents()) {
        if (event.GetEventType() == Aws::SWF::Model::EventType::ActivityTaskCompleted) {
            std::cout
                    << event.GetEventId() << " "
                    << event.GetActivityTaskCompletedEventAttributes().GetResult()
                    << std::endl;

            status = "workDone";

        } else {
            std::cout << event.GetEventId() << std::endl;
        }
    }

}
void MyDecision::respondDecisionTaskCompleted(Aws::SWF::SWFClient &client) {

    if (status == "workDone") {
        requestTerminateWorkflowExecution(client);
    } else {
        scheduleActivityTask(client);
    }

}



void MyDecision::scheduleActivityTask(const Aws::SWF::SWFClient &client) const {


    Aws::Vector<Aws::SWF::Model::Decision> decisions;
    Aws::SWF::Model::Decision decision;

    char activityId[] = "bla";

    Aws::SWF::Model::ScheduleActivityTaskDecisionAttributes scheduleActivityTaskDecisionAttributes;
    Aws::SWF::Model::TaskList taskList;
    scheduleActivityTaskDecisionAttributes.SetTaskList(getTaskList(taskList));
    scheduleActivityTaskDecisionAttributes.SetActivityId(activityId);
    Aws::SWF::Model::ActivityType activityType;
    activityType.SetName("default4");
    activityType.SetVersion("1");
    scheduleActivityTaskDecisionAttributes.SetActivityType(activityType);


    decision.SetScheduleActivityTaskDecisionAttributes(scheduleActivityTaskDecisionAttributes);
    decision.SetDecisionType(Aws::SWF::Model::DecisionType::ScheduleActivityTask);

    std::cout
            << "Schedule activity task " << activityId
            << " on tasklist " << getTaskList(taskList).GetName()
            << std::endl;

    decisions.push_back(decision);

    std::cout
            << "taskToken: "
            << pollForDecisionTaskResult.GetTaskToken()
            << std::endl;


    Aws::SWF::Model::RespondDecisionTaskCompletedRequest respondDecisionTaskCompletedRequest;
    respondDecisionTaskCompletedRequest.SetTaskToken(pollForDecisionTaskResult.GetTaskToken());
    respondDecisionTaskCompletedRequest.SetDecisions(decisions);
    auto outcome = client.RespondDecisionTaskCompleted(respondDecisionTaskCompletedRequest);

    if (!outcome.IsSuccess()) {
        std::cout
                << "failed: exceptionName: "
                << outcome.GetError().GetExceptionName()
                << " message: "
                << outcome.GetError().GetMessage()
                << std::endl;
    }
}

void MyDecision::requestTerminateWorkflowExecution(Aws::SWF::SWFClient &client) {


    Aws::Vector<Aws::SWF::Model::Decision> decisions;
    Aws::SWF::Model::Decision decision;

    char activityId[] = "bla";

    decision.SetDecisionType(Aws::SWF::Model::DecisionType::NOT_SET);
    decisions.push_back(decision);

    std::cout
            << "taskToken: "
            << pollForDecisionTaskResult.GetTaskToken()
            << std::endl;

    Aws::SWF::Model::RespondDecisionTaskCompletedRequest respondDecisionTaskCompletedRequest;
    respondDecisionTaskCompletedRequest.SetTaskToken(pollForDecisionTaskResult.GetTaskToken());
    respondDecisionTaskCompletedRequest.SetDecisions(decisions);
    auto outcome = client.RespondDecisionTaskCompleted(respondDecisionTaskCompletedRequest);

    if (!outcome.IsSuccess()) {
        std::cout
                << "failed: exceptionName: "
                << outcome.GetError().GetExceptionName()
                << " message: "
                << outcome.GetError().GetMessage()
                << std::endl;
    }

    Aws::SWF::Model::TerminateWorkflowExecutionRequest terminateWorkflowExecutionRequest;
    terminateWorkflowExecutionRequest.SetDomain("play");

    terminateWorkflowExecutionRequest.SetWorkflowId(workflowExecution.GetWorkflowId());
    terminateWorkflowExecutionRequest.SetRunId(workflowExecution.GetRunId());

    std::cout
            << "Terminate execution"
            << " on "
            << workflowExecution.GetWorkflowId()
            << " run "
            << workflowExecution.GetRunId()
            << std::endl;

    auto outcome2 = client.TerminateWorkflowExecution(terminateWorkflowExecutionRequest);

    if (!outcome2.IsSuccess()) {
        std::cout
                << "failed: exceptionName: "
                << outcome.GetError().GetExceptionName()
                << " message: \""
                << outcome.GetError().GetMessage()
                << "\""
                << std::endl;
    } else {
        std::cout
                << "success: "
                << outcome.IsSuccess()
                << std::endl;
    }


}
