//
// Created by thomas on 19.09.16.
//

#ifndef SWFPLAY_DECIDER_H
#define SWFPLAY_DECIDER_H


#include <aws/core/utils/Outcome.h>
#include <aws/swf/model/WorkflowExecution.h>
#include <aws/swf/model/PollForDecisionTaskResult.h>
#include <aws/swf/SWFErrors.h>
#include <aws/swf/SWFClient.h>
#include <string.h>

class MyDecision {


    private:
        std::string status;
        const Aws::SWF::Model::PollForDecisionTaskResult &pollForDecisionTaskResult;
        const Aws::SWF::Model::WorkflowExecution &workflowExecution;

    public:
        MyDecision(Aws::SWF::Model::PollForDecisionTaskResult &pollForDecisionTaskResult);
        void respondDecisionTaskCompleted(Aws::SWF::SWFClient &client);

    void scheduleActivityTask(const Aws::SWF::SWFClient &client) const;

    void requestTerminateWorkflowExecution(Aws::SWF::SWFClient &client);
};


#endif //SWFPLAY_DECIDER_H
